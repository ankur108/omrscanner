#import "UIImageView+OpenCV.h"
#import <UIKit/UIKit.h>

@interface CropView : UIView

-(void)showCrop:(CropRect)rect;

@end
