//
//  UIImage+threshhold.m
//  SmartCrop
//
//  Created by Ankur Sharma on 08/04/19.
//  Copyright © 2019 Dmitry Klimkin. All rights reserved.
//

#import "UIImage+OpenCV.h"
#import "UIImage+threshhold.h"

@implementation UIImage (OpenCVBW)

- (UIImage *)findCorrectAnswer:(NSArray *) answers:(int) questions :(int) columns{
  cv::Mat original = [self CVMat];
  cv::Mat grayMat;
  cv::Mat new_image;
  
  
//  UIImage *blackWhiteImage3 = [UIImage imageWithCVMat: original];
  
  if ( original.channels() == 1 ) {
    grayMat = original;
  }
  else {
    grayMat = cv :: Mat( original.rows, original.cols, CV_8UC1 );
    cv::cvtColor( original, grayMat, CV_BGR2GRAY );
  }

  cv::adaptiveThreshold(grayMat, grayMat, 255, cv::ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY_INV, 75, 10);
  
  
  new_image = cv::Mat::zeros( grayMat.size(), grayMat.type() );

  grayMat.convertTo(new_image, -1, 1.4, -50);
  
//  UIImage *blackWhiteImage2 = [UIImage imageWithCVMat: grayMat];
//
//  return blackWhiteImage2;

  std::vector<std::vector<cv::Point>> contours;
  
  cv::findContours(new_image, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
  
  new_image.release();
  
  std::vector<std::vector<cv::Point>> qCountr = std::vector<std::vector<cv::Point>>();
  
  int widthAllowed = 30;
  
  //find bubbles
  for (size_t i = 0; i < contours.size(); i++) {
    cv::Rect rect= cv::boundingRect(contours[i]);
    float ar = rect.width / float(rect.height);
    
    if (rect.width > widthAllowed && rect.height > widthAllowed && ar > 0.8 && ar < 1.2){
      qCountr.push_back(contours[i]);
    }
  }
  
  
  
  //sort top to down
  std::vector<std::vector<cv::Point>> row = std::vector<std::vector<cv::Point>>();
  
  
  
  
  //for row top to down
  for (size_t i = 0; i < qCountr.size(); i++){
    row.push_back(qCountr[qCountr.size() - 1 - i]);
  }
  
 
  
  
  
  int quest = questions;//200;
  int qstack = columns;//5;
  int qCol = 1;//for 1 column
  int option = 4 + qCol;//1 for question
  float threshValue = 2 / CGFloat(3);
  //column sync
  
  if (row.size() >= 1000){
    quest = 200;
    qstack = 5;
    threshValue = 6 / CGFloat(10);
  }else if (row.size() >= 250){
    quest = 50;
    qstack = 2;
    threshValue = 4 / CGFloat(10);
  }else if (row.size() >= 100){
    quest = 20;
    qstack = 1;
    threshValue = 1 / CGFloat(2);
  }else{
    quest = 10000;
  }
  
  if (row.size() < quest){
    UIImage *blackWhiteImage2 = [UIImage imageWithCVMat: grayMat];
    return blackWhiteImage2;
  }
  
  for (size_t r = 0; r < quest; r++){
    
    size_t offset = r * option;
    size_t limit = offset + option - 1;
    
    for (size_t i = offset; i < limit; i++){
      for (size_t j = offset; j < limit + offset - i ; j++ ){
          if ([self isGreater:row[j] : row[j+1]]){
            std::vector<cv::Point> t = row[j];
            row[j] = row[j + 1];
            row[j + 1] = t;
          }
        }
      }
  }

  
  
//  std::vector<std::vector<cv::Point>> temp = std::vector<std::vector<cv::Point>>();
//
//  for (size_t i = 0; i < row.size(); i = i + 5){
//    temp.push_back(row[i]);
//  }
  
  std::vector<std::vector<cv::Point>> selected = std::vector<std::vector<cv::Point>>();
  
  for (size_t r = 0; r < quest;r++){
    size_t offset = r * option;
    size_t limit = offset + option ;
    offset = offset + qCol;
    for (size_t j = offset  ; j < limit; j++){
      //  add mask
      
        new_image = cv::Mat::zeros( grayMat.size(), grayMat.type() );
      
        grayMat.convertTo(new_image, -1, 1.4, -50);
      
      
      std::vector<std::vector<cv::Point>> temp2 = std::vector<std::vector<cv::Point>>();
      
      temp2.push_back(row[j]);
      
        cv::Mat mask = cv::Mat::zeros( new_image.size(), new_image.type() );
      
        cv::drawContours(mask, temp2, -1,255, -1);
      
      float thresh = cv::countNonZero(mask) * threshValue;
      
        cv::bitwise_and(new_image, new_image, mask, mask);
      
      int maskedBit = cv::countNonZero(mask);
      
      new_image.release();
      
      if (maskedBit > thresh){
        selected.push_back(row[j]);
        int answer = int(j - offset + 1);
        
        cv::drawContours(original, temp2, -1, cv::Scalar(0,255,0), 4);
        
        //for correct answer
        int multiplier = (j % option) % qstack;
        
        int correctAnswerIndex = (quest / qstack) * multiplier + r;
        
        int correct = [[answers objectAtIndex:correctAnswerIndex] intValue] ;
        
        if (correct != answer){
          temp2.push_back(row[offset + correct - 1]);
          
          //wrong answer
          cv::drawContours(original, temp2, 0, cv::Scalar(255,0,0), 4);
          
          
          //correct answer
          cv::drawContours(original, temp2, 1, cv::Scalar(0,255,0), 4);
          
        }
        
        
        NSLog(@"Question %d -> answer choosen %lu", correctAnswerIndex, j - offset + 1);
      }
    }
  }
  
  
//  cv::drawContours(original, row, -1, cv::Scalar(0,255,0), 4);
  

  UIImage *blackWhiteImage = [UIImage imageWithCVMat: original];
  
  
  
  
  return blackWhiteImage;
}

-(BOOL) isGreater:(std::vector<cv::Point>)first :(std::vector<cv::Point>)second {
  
  int x1 = cv::boundingRect(first).x;
  int x2 = cv::boundingRect(second).x;
  
  
  if (x1 > x2){
    return YES;
  }
  return FALSE;
}


@end
