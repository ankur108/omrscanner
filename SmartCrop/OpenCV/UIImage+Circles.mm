//
//  UIImage+threshhold.m
//  SmartCrop
//
//  Created by Ankur Sharma on 08/04/19.
//  Copyright © 2019 Dmitry Klimkin. All rights reserved.
//

#import "UIImage+OpenCV.h"
#import "UIImage+threshhold.h"

@implementation UIImage (OpenCVBW)

- (UIImage *)circleCount{
  cv::Mat original = [self CVGrayscaleMat];
  cv::Mat grayMat;
  cv::Mat new_image;
  
  std::vector<std::vector<cv::Point>> contours;
  
  
  if ( original.channels() == 1 ) {
    grayMat = original;
  }
  else {
    grayMat = cv :: Mat( original.rows, original.cols, CV_8UC1 );
    cv::cvtColor( original, grayMat, CV_BGR2GRAY );
  }
  
  //  cv:: threshold(grayMat, grayMat, 0, 255, CV_THRESH_BINARY_INV | CV_THRESH_OTSU);
  
  
  UIImage *blackWhiteImage0 = [UIImage imageWithCVMat: grayMat];
  
  
  cv::findContours(original, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
  
  cv::drawContours(original, contours, -1, cv::Scalar(0,255,0));
  
  UIImage *blackWhiteImage = [UIImage imageWithCVMat: original];
  
//  new_image = cv::Mat::zeros( grayMat.size(), grayMat.type() );
//
//  grayMat.convertTo(new_image, -1, 1.4, -50);
//  grayMat.release();
//
//  UIImage *blackWhiteImage = [UIImage imageWithCVMat: new_image];
//
//  new_image.release();
//
  return blackWhiteImage;
}

@end
