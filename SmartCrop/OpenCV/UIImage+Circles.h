
//
//  UIImage+Circles.h
//  SmartCrop
//
//  Created by Ankur Sharma on 08/04/19.
//  Copyright © 2019 Dmitry Klimkin. All rights reserved.
//



#import <UIKit/UIKit.h>

@interface UIImage (OpenCVBW)

- (UIImage *)circleCount;

@end
