//
//  ChooseViewController.swift
//  SmartCrop
//
//  Created by Ankur Sharma on 12/04/19.
//  Copyright © 2019 Dmitry Klimkin. All rights reserved.
//

import UIKit

class ChooseViewController: UIViewController {

  var image: UIImage?
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

  @IBAction func camera(){
    let picker = UIImagePickerController()
    picker.delegate = self
    picker.sourceType = .camera
//    picker.allowsEditing = true
    self.present(picker, animated: true, completion: nil)
  }
  
  @IBAction func gallery(){
    let picker = UIImagePickerController()
    picker.delegate = self
    picker.sourceType = .photoLibrary
    self.present(picker, animated: true, completion: nil)
  }
  
}


extension ChooseViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
    defer{
      picker.dismiss(animated: true, completion: nil)
    }
    
    guard let image =
      (info[UIImagePickerControllerEditedImage] as? UIImage) ?? (info[UIImagePickerControllerOriginalImage] as? UIImage) else
    {
      return
    }
    
    self.image = image.fixOrientation()
    self.performSegue(withIdentifier: "imager", sender: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "imager", let vc = segue.destination as? ImagerViewController{
      vc.image = self.image
      vc.isProcessingAllowed = true
    }
  }
}


extension UIImage {
  func fixOrientation() -> UIImage {
    if self.imageOrientation == UIImageOrientation.up {
      return self
    }
    
    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
    self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
    let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    return normalizedImage;
  }
}
